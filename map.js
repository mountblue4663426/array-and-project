export function map(elements, cb) {
  if (!Array.isArray(elements)) {
    return undefined;
  }

  let mappedArray = [];

  for (let index = 0; index < elements.length; index++) {
    mappedArray.push(cb(elements[index]));
  }

  return mappedArray;
}
