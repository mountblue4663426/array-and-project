export function reduce(elements, cb, startingValue) {
  if (Array.isArray(elements) == false) {
    return undefined;
  }

  if (elements.length == 0) {
    return startingValue;
  }

  let startingIndex = 0;
  let accumulator;

  if (startingValue == undefined) {
    accumulator = elements[startingIndex];
    startingIndex++;
  } else {
    accumulator = startingValue;
  }

  while (startingIndex < elements.length) {
    accumulator = cb(accumulator, elements[startingIndex]);
    startingIndex++;
  }

  return accumulator;
}
