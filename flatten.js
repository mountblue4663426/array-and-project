export function flatten(elements) {
  if (!Array.isArray(elements)) {
    return undefined;
  }

  let flattendArray = [];

  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      let recursiveArray = flatten(elements[index]);

      for (let recIndex = 0; recIndex < recursiveArray.length; recIndex++) {
        flattendArray.push(recursiveArray[recIndex]);
      }
    } else {
      flattendArray.push(elements[index]);
    }
  }

  return flattendArray;
}
