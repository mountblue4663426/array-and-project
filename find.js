export function find(elements, cb) {
  if (!Array.isArray(elements)) {
    return undefined;
  }

  let foundValue;

  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      foundValue = elements[index];
      break;
    }
  }

  return foundValue;
}
