import { items } from "../data/items.js";
import { find } from "../find.js";

function callback(element) {
  return element % 2 == 0;
}

let result = find(items, callback);
console.log(result);
