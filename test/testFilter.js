import { items } from "../data/items.js";
import { filter } from "../filter.js";

function callback(element) {
  return element % 2 == 0;
}

let result = filter(items, callback);
console.log(result);
