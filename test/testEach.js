import { items } from "../data/items.js";
import { each } from "../each.js";

function callback(element, index) {
  console.log(`Index: ${index}, element: ${element}`);
}

each(items, callback);
