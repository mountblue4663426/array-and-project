import { items } from "../data/items.js";
import { reduce } from "../reduce.js";

function callback(accumulator, element) {
  return accumulator + element;
}

let result = reduce(items, callback);
console.log(result);
