import { items } from "../data/items.js";
import { map } from "../map.js";

function callback(element) {
  return element * element;
}

let result = map(items, callback);
console.log(result);
